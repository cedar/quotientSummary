//Initial software, [Manolescu-Goujot, Goasdoué, Guzewicz], Copyright C Inria and Rennes 1 University, see the license available at https://gitlab.inria.fr/cedar/RDFQuotient/blob/master/LICENSE.txt

package fr.inria.cedar.RDFQuotient.datastructures;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class Long2Long {
	private static final Logger LOGGER = Logger.getLogger(Long2Long.class.getName());

	static {
		LOGGER.setLevel(Level.INFO);
	}

	// from the node to the ID of its clique
	protected final HashMap<Long, Long> map;
	// from the ID of a clique, to the list of IDs of all the nodes
	protected final HashMap<Long, HashSet<Long>> inverse;

	public Long2Long() {
		map = new HashMap<>();
		inverse = new HashMap<>();
	}

	public Long2Long(Long2Long original) {
		map = new HashMap<>(original.map);
		inverse = new HashMap<>(original.inverse);
	}

	public Long get(long node) {
		return map.get(node);
	}

	public HashSet<Long> getInverse(Long l) {
		return inverse.get(l);
	}

	/**
	 * Returns true if node had a previous representative who now becomes a
	 * representative of no one.
	 * @param k
	 * @param v
	 */
	public void put(Long k, Long v) {
		//LOGGER.debug("Long2Long: upon entering put " + clique + " on " + node + ": " + this.display());
		boolean res = false;

		Long previous = map.get(k);

		if (previous != null){
			HashSet<Long> inversePrev = inverse.get(previous);
			if (inversePrev == null){
				//LOGGER.debug("Long2Long: Problem " + this.display());
				throw new IllegalStateException("Map has " + previous + " on " + k + " but nothing in inverse for " + previous);
			}
			inversePrev.remove(k);
			//LOGGER.debug("Long2Long: " + k + " no  longer mapped to " + previous);
			if (inversePrev.isEmpty()){
				//LOGGER.debug("Long2Long: No one is represented by " + previous + " any more!");
				res = true;
				inverse.remove(previous);
			}
		}
		map.put(k, v);

		HashSet<Long> keysForV = inverse.computeIfAbsent(v, k1 -> new HashSet<>());
		keysForV.add(k);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		if (!map.keySet().isEmpty()) {
			sb.append("\n----------: \n");
			for (Long key: map.keySet()) {
				sb.append("#").append(key).append("|");
				//sb.append(RDF2SQLEncoding.dictionaryDecode(key) + "|");
				sb.append(map.get(key)).append(" ");
			}
			sb.append("Inverse:");
			for (Long value: inverse.keySet()) {
				sb.append("*").append(value).append("|");
				for (Long key: inverse.get(value))
					sb.append(key).append(",");
					//sb.append(RDF2SQLEncoding.dictionaryDecode(key) + ",");
				sb.setLength(sb.length() - 1);
				sb.append("| ");
			}
		}
		return sb.toString();
	}

	/**
	 * value 1, value 2
	 * all keys previously associated to value 1 should now map to value 2
	 * entries previously associated to value 2 stay the same
	 *
	 */
	public void replaceValue(Long v1, Long v2) {
		//LOGGER.debug("Trying to replace value " + v1 + " with " + v2 + " in:");
		HashSet<Long> keysWithV1 = inverse.get(v1);
		if (keysWithV1 != null){
			HashSet<Long> keysWithV2 = inverse.computeIfAbsent(v2, k -> new HashSet<>());
			for (Long l: keysWithV1) {
				keysWithV2.add(l);
				map.put(l, v2); // this erases (l, v1)
			}
		}
		inverse.remove(v1);
	}

	/** remove the value on this key, and the inverse mapping
	 *
	 */
	public void remove(Long key) {
		Long value = map.get(key);
		if (value != null) {
			map.remove(key);
			HashSet<Long> a = inverse.get(value);
			a.remove(key);
			if (a.isEmpty())
				inverse.remove(value);
		}
	}

	public Set<Long> getKeys() {
		return map.keySet();
	}

	public long numberOfKeys() {
		return map.keySet().size();
	}

	public long numberOfDistinctValues() {
		/*HashSet<Long> values = new HashSet<>();
		for (Long key: map.keySet()) {
			Long val = map.get(key);
			if (!values.contains(val))
				values.add(val);
		}
		return values.size();*/
		return inverse.keySet().size();
	}
}
